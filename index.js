const express = require('express')

const app = express()

const port = 5000

app.use(express.json())

app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`server is running at localHost: ${port}`))

app.get("/home", (request, response) => {
	response.send('Welcome to the home page.')
})

let users = [{
	username: "johndoe",
	password: "johndoe1234"
}]

app.get("/users", (request, response) => {
	response.send(users)
})

app.delete("/delete-user", (request, response) => {
	let message

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice([i],1)
			message = `User ${request.body.username} has been deleted!`

			break
		} else {
			message = `User does not exist!`
		}
	}

	response.send(message)
})